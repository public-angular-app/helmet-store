import { Component } from '@angular/core';

@Component({
  selector: 'app-new-products',
  templateUrl: './new-products.component.html',
  styleUrl: './new-products.component.css',
})
export class NewProductsComponent {
  currentIndex = 0;
  products = [
    { name: 'HJC CL-17 Captain America', price: '$350.5' },
    { name: 'ZEUS Z-806 New Supertech I150 Matt', price: '$350.5' },
    { name: 'Arai Tour Cross 3 - Detour Red', price: '$350.5' },
  ];

  next() {
    this.currentIndex = (this.currentIndex + 1) % this.products.length;
  }

  prev() {
    this.currentIndex =
      (this.currentIndex - 1 + this.products.length) % this.products.length;
  }
}
